var musicMenu = document.querySelector('.music-menu');
var musicList = document.querySelector('.music-list');
var musicClosed = document.querySelector('.music-list__close')

var closed = 'music-list__closed'
var open = 'music-list__open'

musicMenu.addEventListener('click', function(){
    musicList.classList.remove('music-list__closed');
    musicList.classList.add('music-list__open');
});

musicClosed.addEventListener('click', function(){
    musicList.classList.add('music-list__closed');
})

var searchBox = document.querySelector('#search-box');

console.log(searchBox)

searchBox.addEventListener('click', () =>{
    searchBox.classList.add('music-search__box-opened');
})

// function toggleSearch(){
//     document.querySelector('#search-box').classList('music-search__box-opened');
// }